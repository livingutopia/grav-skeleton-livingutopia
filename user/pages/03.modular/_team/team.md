---
title: Team
team:
  - name: Bakira
    image: bakira.jpg
  - name: Pia
    email: pia.b@utopival.org
    image: pia.jpg
  - name: Helena
    image: helena.jpg
  - name: Juli
    image: juli.jpg
  - name: Ruth
    email: ruth@utopival.org
    image: ruth.jpg
  - name: findus
    email: findus@livingutopia.org
    image: laura.jpg
  - name: Nico
    image: nico.jpg
  - name: chandi
    email: chandi@livingutopia.org
    image: chandi.jpg
  - name: Nadine
    image: nadine.jpg
  - name: Tobi
    image: tobi.jpg
  - name: Tom
    image: tom.jpg
---

## Team `team.md`