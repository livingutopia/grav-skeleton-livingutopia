---
title: Programm 2017
times: 
 - 07:00
 - 08:00
 - 09:00
 - 10:00
 - 11:00
 - 12:00
 - 13:00
 - 13:00
 - 14:00
 - 15:00
 - 16:00
 - 17:00
 - 18:00
 - 19:00
 - 20:00
 - 21:00
 - 22:00
 - OpenEnd

# categories:
#     event-1:
#         color: 

days: 
  - name: Sonntag
    events:
      - title: Einbindungsaktivitäten, Ankommen, Zelt aufbauen, Schnibbeln, Restliche ToWupps
        start: 14:30
        end: 18:00
        category: event-3

      - title: Abendessen
        start: 18:00
        end: 19:00
        category: event-4

      - title: Erstes Plenum
        start: 19:00
        end: 20:00
        category: event-1

      - title: Bärchengruppen
        start: 20:00
        end: 21:00
        category: event-6

      - title: Abendprogramm
        start: 21:00
        end: 22:00
        category: event-5

  - name: Montag
    events:
      - title: Morgenprogramm
        start: 7:00
        end: 8:00
        category: event-1

      - title: Frühstück
        start: 8:00
        end: 9:00
        category: event-4

      - title: Anarchistisches Mogenplenum
        start: 9:30
        end: 10:30
        category: event-6
        class: text-small

      - title: Kennenlernen
        start: 10:30
        end: 11:30
        category: event-1

      - title: Impulsvortrag
        start: 11:30 
        end: 12:00
        category: event-2
        class: noTime

      - title: Schnibbeln / <br />Re-&amp;Pro-flexion
        start: 12:00
        end: 13:30
        category: event-1

      - title: Mittagessen
        start: 13:30
        end: 14:30
        category: event-4

      - title: Workshops<br />Open Space
        start: 15:00
        end: 17:00
        category: event-2

      - title: Schnibbeln &amp; Utopie-Kreis
        start: 17:00
        end: 19:00
        category: event-1

      - title: Abendessen
        start: 19:00
        end: 20:00
        category: event-4

      - title: Bärchengruppen
        start: 20:00
        end: 21:00
        category: event-6

      - title: Konzert
        start: 21:00
        end: 22:00
        category: event-5

  - name: Dienstag
    events:
    - title: Morgenprogramm
      start: 7:00
      end: 8:00
      category: event-1
      
    - title: Frühstück
      start: 8:00
      end: 9:00
      category: event-4
      
    - title: Anarchistisches Mogenplenum
      start: 9:30
      end: 10:30
      category: event-6
      class: text-small

    - title: Workshops<br />Open Space<br />Schnibbeln
      start: 11:00
      end: 13:30
      category: event-2
      
    - title: Mittagessen
      start: 13:30
      end: 14:30
      category: event-4
      
    - title: Workshops<br />Open Space
      start: 15:00
      end: 17:00
      category: event-2
      
    - title: Open Space<br />Schnibbeln
      start: 17:00
      end: 19:00
      category: event-2
      
    - title: Abendessen
      start: 19:00
      end: 20:00
      category: event-4
      
    - title: Bärchengruppen
      start: 20:00
      end: 21:00
      category: event-6
      
    - title: Podium
      start: 21:00
      end: 22:00
      category: event-1
      
    - title: Open Stage
      start: 22:00
      end: OpenEnd
      category: event-5


  - name: Mittwoch
    events:
    - title: Morgenprogramm
      start: 7:00
      end: 8:00
      category: event-1
      
    - title: Frühstück
      start: 8:00
      end: 9:00
      category: event-4
      
    - title: Anarchistisches Mogenplenum
      start: 9:30
      end: 10:30
      category: event-6
      class: text-small
      
    - title: Workshops<br />Open Space<br />Schnibbeln
      start: 11:00
      end: 13:30
      category: event-2
      
    - title: Mittagessen
      start: 13:30
      end: 14:30
      category: event-4
      
    - title: Workshops<br />Open Space
      start: 15:00
      end: 17:00
      category: event-2
      
    - title: Open Space<br />Schnibbeln
      start: 17:00
      end: 19:00
      category: event-2
      
    - title: Abendessen
      start: 19:00
      end: 20:00
      category: event-4
      
    - title: Bärchengruppen
      start: 20:00
      end: 21:00
      category: event-6
      
    - title: Konzert
      start: 21:00
      end: 22:00
      category: event-5
      
    - title: DJ / Party
      start: 22:00
      end: OpenEnd
      category: event-5


  - name: Donnerstag
    events:
    - title: (Schweigemorgen)<br />Brunch vorbereiten
      start: 7:00
      end: 10:30
      category: event-1
      
    - title: Brunch
      start: 10:30
      end: 12:00
      category: event-4
      
    - title: Konzert (Schweigen brechen)
      start: 12:00
      end: 13:30
      category: event-5
      
    - title: Open Stage
      start: 13:30
      end: 15:00
      category: event-5
      
    - title: Projektaustausch
      start: 15:00
      end: 16:00
      category: event-1
      
    - title: Open Space<br />Schnibbeln
      start: 16:00
      end: 18:00
      category: event-2
      
    - title: Abendessen
      start: 18:00
      end: 19:00
      category: event-4
      
    - title: Bärchengruppen
      start: 19:00
      end: 20:00
      category: event-6
      
    - title: Open Mic
      start: 20:00
      end: 22:00
      category: event-5
  
  
  - name: Freitag
    events:
    - title: Morgenprogramm
      start: 7:00
      end: 8:00
      category: event-1
      
    - title: Frühstück
      start: 8:00
      end: 9:00
      category: event-4
      
    - title: Anarchistisches Mogenplenum
      start: 9:30
      end: 10:00
      category: event-6
      class: noTime
      
    - title: Workshops<br />Open Space<br />Schnibbeln
      start: 10:00
      end: 13:30
      category: event-2
      
    - title: Mittagessen
      start: 13:30
      end: 14:30
      category: event-4
      
    - title: Workshops<br />Open Space
      start: 15:00
      end: 17:00
      category: event-2
      
    - title: Open Space<br />Schnibbeln
      start: 17:00
      end: 19:00
      category: event-2
      
    - title: Abendessen
      start: 19:00
      end: 20:00
      category: event-4
      
    - title: Bärchengruppen
      start: 20:00
      end: 21:00
      category: event-6
      
    - title: Abendplenum
      start: 21:00
      end: 22:00
      category: event-1
      
    - title: Letzter Abend
      start: 22:00
      end: OpenEnd
      category: event-5

  - name: Samstag
    events:
    - title: Morgenprogramm
      start: 7:00
      end: 8:00
      category: event-1
      
    - title: Frühstück
      start: 8:00
      end: 9:00
      category: event-4
      
    - title: Packen und Aufräumen
      start: 9:00
      end: 11:00
      category: event-3
      
    - title: Abschiedsplenum
      start: 11:00
      end: 11:30
      category: event-1
      class: noTime
      
    - title: Verabschiedung
      start: 11:30
      end: 12:30
      category: event-3
        
---

# Schedule `schedule.md`