---
title: Features
class: small
features:
    - header: tauschlogikfrei
      icon: fa fa-asterisk
      text: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua TODO
      url: https://livingutopia.org/geldfrei/

    - header: vegan
      icon: fa fa-heart
      text: Die vegane Lebensweise vereint einige Aspekte. Wir gestalten aus verschiedenen Gründen alle Veranstaltungen vegan.
      url: https://livingutopia.org/vegan/

    - header: drogenfrei
      icon: fa fa-eye
      text: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua TODO
      url: https://livingutopia.org/drogenfrei/

    - header: ökologisch
      icon: fa fa-leaf
      text: Wir haben den Anspruch unsere Projekte sowie jegliches andere Wirken so ökologisch wie möglich zu gestalten.
      url: https://livingutopia.org/oekologisch/
    
    - header: solidarisch
      icon: fa fa-globe
      text: Solidarität spielt in unseren Projekten auf verschiedenen Ebenen eine wichtige Rolle.
      url: https://livingutopia.org/solidarisch/
    

---
# Features `features.md`