---
title: Home
body_classes: title-center title-h1h2
---

# Willkommen
## Installation erfolgreich...

!!! Glückwunsch, du hast Grav mit dem living utopia Theme erfolgreich installiert!


### Links
- Viel Wissen über Grav: [learn.getgrav.org](https://learn.getgrav.org) 
- Das Adminmenü: [/admin](/admin) 
- Eine Übersicht der Komponenten für modulare Seiten: [hier](/modular) 